-- The top level class.
-- Create subclasses using:
-- Object:Subclass(className, constructor)
-- constructor is an optional paramter.
-- Example:
-- local Foo = Object:Subclass("Foo")
-- local foo = Foo:new()
-- Object.print(foo)

-- Example With constructor defined:
-- local Apple = Object:Subclass("Apple", function(this, color)
--     print("calling Apple constructor with color = " .. color)
--     this.Color = color
-- end)

-- Define functions for the subclass like this:
-- function Apple:GetColor()
--     return self.Color
-- end
--
-- Subclasses will inherit from the superclass
--
-- Make a deeper subclass that calls a super constructor like this:
--
-- local RottenApple = Apple:Subclass("RottenApple", function(this, state)
--      Apple.Constructor(this, "Brown")
--      this.State = state
-- end)

local Object = {}

Object.ClassName = "Object"
Object.Class = Object
Object.Constructor = function() end
Object.__index = Object
Object.SINGLE_LINE = {true}
local callingSuper = {true}

-- The Constructor for the top level class Object
function Object:new(...)
    local args = {...}
    local this;
    if args[1] == callingSuper then this = args[2]
    else this = {} end
    setmetatable(this, self)
    self.__index = self
    this.Classes = {Object = true}
    this.ClassName = Object.ClassName
    return this
end

-- Create a class with the given name that is a subclass
-- of the class this function is called on and with the
-- given constructor (optional).
--
-- The super class constructors will be called up the chain, passing no
-- parameters (see below if you want to pass parameters)
--
-- If no constructor is passed, then a default constructor that only calls
-- the default super constructor will be defined
--
-- If you want to call the superclass constructor with parameters:
-- The constructor should call it explicitly. For example, if the
-- superclass is A, then you could say
-- A.Constructor(this, params...)
-- to call A's constructor
function Object:Subclass(className, constructor)
    local Child = {}
    local Parent = self.Class
    Child.ClassName = className
    Child.Parent = Parent
    Child.Class = Child
    if constructor == nil then
        constructor = function(this) end
    end
    assert(type(constructor) == "function",
        tostring(constructor).." is not a valid constructor because it is not a function.")
    Child.Constructor = constructor
    
    -- This makes the Child table look up any missing
    -- fields in Parent (i.e. Child inherits from Parent)
    setmetatable(Child, Parent)
    Child.__index = Child

    -- Constructor for Child returns an object that inherits from Parent
    function Child:new(...)
        local args = {...}
        local this;
        if args[1] == callingSuper then
            this = Parent:new(args[1], args[2])
        else
            this = Parent:new(callingSuper, {})
        end
        setmetatable(this, self)
        self.__index = self
        this.Classes[className] = true
        this.ClassName = className
        if args[1] ~= callingSuper then
            constructor(this, ...)
        end
        return this
    end
    return Child
end

-- Returns a string made up of n spaces
function getSpaces(n)
    local s = ""
    for i = 1, n do
        s = s .. " "
    end
    return s
end

-- Returns a nicely formatted string representing the contents of a table
-- If multiline is not explicitly false, then the elements will each be
-- on their own line and nested tables will be indented.
-- level is only used in the recursive calls to keep track of indentation
function tablestring(t, multiline, level)
    if type(t) == "table" then
        if multiline == nil then multiline = true end
        level = level or 1
        local s = "{"
        if multiline then s = s .. "\n" end
        local isEmpty = true
        for k,v in pairs(t) do
            isEmpty = false
            if multiline then s = s .. getSpaces(level * 2) end
            s = s .. k .. ": "
            if type(v) == "table" and Object.isobj(v) == false then
                s = s .. tablestring(v, multiline, level + 1)
            else
                local sep = multiline and ",\n" or ", "
                s = s .. Object.tostring(v, multiline) .. sep
            end
        end
        if not isEmpty then
            if not multiline then
                s = string.sub(s, 1, string.len(s) - 2)
            else
                s = string.sub(s, 1, string.len(s) - 2)..string.sub(s, string.len(s)) 
            end
        end
        if multiline then s = s .. getSpaces((level - 1) * 2) end
        s = s .. "}"
        if level > 1 then
            s = s .. ","
            if multiline then s = s .. "\n"
            else s = s .. " " end
        end
        return s
    end
end

-- Return true if the parameter is an Object or a descendent of Object
-- and false otherwise (i.e. other tables, numbers, and strings are not)
function Object.isobj(o)
    return type(o) == "table" and type(o.IsKindOfClass) == "function"
end

-- Returns the result of ToString() if v is an Object,
-- tablestring() if the object is a table
-- and tostring() otherwise.
-- If multiline is false, then tables
-- will be in the same line, otherwise they will be printed in
-- multiple lines with nested tables indented.
function Object.tostring(v, multiline)
    if Object.isobj(v) then return v:ToString(multiline) end
    if type(v) == "table" then return tablestring(v, multiline) end
    return tostring(v)
end

-- Concatenates the result of Object.tostring() for each parameter
-- and prints the result.
--
-- Note: if there are multiple parameters
-- and the last parameter is nil, then that nil will not be printed
--
-- Examples:
-- Object.print(nil) WILL print the nil because nil is the only param
-- Object.print(6, nil) WILL NOT print the nil since it is the last param
-- Object.print(6, nil, "") WILL print the nil since it is not the last param
--
-- Unlike print(6, nil, 7), Object.print(6, nil, 7) will not
-- insert tabs between the parameters, so users can separate components
-- as desired.
--
-- tables will be printed in multiline mode by default
-- The PrintMultiline() method determines whether Objects are printed
-- in multiline mode (unless the class overrides it, the default is false)
-- If you want to force objects and tables to be printed in single line mode,
-- pass Object.SINGLE_LINE as the last parameter.
-- Note: If Object.SINGLE_LINE is the last parameter, it will not be printed
function Object.print(...)
    local args = {...}
    if #args == 0 then
        print(...)
    else
        local s = ""
        local single = false
        if args[#args] == Object.SINGLE_LINE then
            single = true
            args[#args] = nil -- remove so it won't print
        end
        for i = 1,#args do
            local multiline = not single
            -- If the outer object is an Object, then the default
            -- is whatever PrintMultiline() returns (false unless
            -- the class overrides it)
            if Object.isobj(args[i]) then
                -- only multiline if default is true and single line parm not set
                multiline = args[i]:PrintMultiline() and multiline
            elseif not type(args[i]) == "table" then
                multiline = false
            end
            s = s..Object.tostring(args[i], multiline)
        end
        print(s)
    end
end

-- Whether to print in multiline mode by default
-- Subclasses can override this to return true as desired
function Object:PrintMultiline()
    return false
end

-- Returns true if the objects are equal and false otherwise
function Object:Equals(obj)
    return self == obj
end
    
-- Returns true if this object is an instance of the class given by the
-- classname parameter or is an instance of a subclass of that class.
function Object:IsKindOfClass(className)
    return self.Classes[className] == true
end

-- Returns the class name of this object
function Object:GetClassName()
    return self.ClassName
end

-- Returns the Class of this object
function Object:GetClass()
    return self.Class
end

-- The string representation of the object
-- Default is based on tostring() returning a string in format:
-- table: 0x55856a7f5660
-- if this format were to change, this would break
function Object:ToString()
    local s = tostring(self)
    return self:GetClassName()..":"..s:sub(7)
end

return Object

--Example Usage
-- local Foo = Object:Subclass("Foo")
-- local foo = Foo:new()
-- Object.print(foo)
-- Object.print(6, nil, 7)
-- local Bar = Foo:Subclass("Bar")
-- local b = Bar:new()
-- Object.print(b)
-- local Apple = Object:Subclass("Apple", function(this, color)
--     print("calling Apple constructor with color = " .. color)
--     this.Color = color
-- end)

-- function Apple:GetColor()
--     return self.Color
-- end

-- local app = Apple:new("red")
-- print(app)
-- print(app:GetColor())
-- print(app:IsKindOfClass(Apple.ClassName))
-- print(app:IsKindOfClass(Object.ClassName))
-- print(app:IsKindOfClass("Ball"))

-- print("Rotten Apple:")
-- local RottenApple = Apple:Subclass("RottenApple", function(this, state)
--     Object.print("In RottenApple Constructor with state = ", state)
--     Apple.Constructor(this, "Brown")
--     this.State = state
-- end)

-- function RottenApple:GetState()
--     return self.State
-- end

-- local RottenGoldenApple = RottenApple:Subclass("RottenGoldenApple", function(this, color, state)
--     Object.print("In RottenGoldenApple Constructor with color = ", color, " and state = ", state)
--     RottenApple.Constructor(this, state)
--     this.Color = "Golden " .. color .. "ish " .. this.Color
-- end)

-- local rga = RottenGoldenApple:new("Pink", "Ripe")

-- Object.print("rga color = ", rga:GetColor())
-- Object.print("rga state = ", rga:GetState())

-- local A = Object:Subclass("A", function(this, x)
--     Object.print("A Constructor", x)
--     this.x = x
    
-- end)

-- local B = A:Subclass("B", function(this, x)
--     Object.print("B Constructor", x)
--     A.Constructor(this, x)
-- end)

-- local C = B:Subclass("C", function(this, x)
--     Object.print("C Constructor", x)
--     B.Constructor(this, x)
-- end)

-- local ac = C:new(9)
-- Object.print(ac)

-- local rotApp = RottenApple:new("Decaying")
-- print(rotApp:GetColor())
-- print(rotApp:IsKindOfClass(Object.ClassName))
-- print(rotApp:IsKindOfClass(Apple.ClassName))
-- print(rotApp:IsKindOfClass(RottenApple.ClassName))
-- print(rotApp:IsKindOfClass("Baller"))
-- print(rotApp:GetState())
-- print(rotApp:GetClassName())

-- print(Object.isobj(rotApp))
-- print(Object.isobj("hi"))
-- print(Object.isobj(6))
-- local tstTab = {x = 7, y = 9, z = {m = 9, p = "hi"}}
-- print(Object.tostring(tstTab, false))
-- print(Object.tostring(tstTab, true))
-- Object.print({x = {5, 3, 4}, "A", {g = 9, k = "K"}})
-- Object.print({x = {5, 3, 4}, "A", {g = 9, k = "K"}}, Object.SINGLE_LINE)Object.print({})
-- Object.print()
-- Object.print({one = {}, two = "hi", four = { five = {6}}})
-- Object.print()
-- Object.print({}, Object.SINGLE_LINE)