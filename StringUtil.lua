local Object = require(script.Parent.Object)

local StringUtil = Object.Subclass("StringUtil")

function StringUtil.split(str, delim)
    local s = {}
    for i in string.gmatch(str, "([^"..delim.."]+)") do
       table.insert(s, i)
    end
    return s
end

function StringUtil.join(arr, delim)
    local s = ""
    for i,str in ipairs(arr) do
        s = s .. str
        if i < #arr then
            s = s .. delim
        end
    end
    return s
end

function StringUtil.trim(s)
    return s:match "^%s*(.-)%s*$"
end

return StringUtil
