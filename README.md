# README #

### Util ###

This is a library that implements a class system for lua and also provides some convenient classes.

## Roblox Setup ##

If used in Roblox, simply add a folder containing all these scripts to the ReplicatedStorage

Create subclasses using:

    Object:Subclass(className, constructor)
	
constructor is an optional paramter.
Example:

    local Foo = Object:Subclass("Foo")
    local foo = Foo:new()
    Object.print(foo)

Example With constructor defined:

    local Apple = Object:Subclass("Apple", function(this, color)
       print("calling Apple constructor with color = " .. color)
       this.Color = color
    end)

Define functions for the subclass like this:

    function Apple:GetColor()
       return self.Color
    end

Subclasses will inherit from the superclass
Make a deeper subclass like this:

    local RottenApple = Apple:Subclass("RottenApple", function(this, state)
        Apple.Constructor(this, "Brown")
        this.State = state
    end)