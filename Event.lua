local Object = require(script.Parent.Object)
local Set = require(script.Parent.Set)

-- This class represents an event that can be fired and responded to.
-- Use the Fire method to fire the event, passing data desired, and
-- all the functions that have been connected with the Connect method
-- will be called and the data will be passed.
local Event = Object:Subclass("Event", function(this, listeners)
	-- functions to call when the event is fired
	this.listeners = Set:new(listeners)
end)

-- When the event occurs, call this function to call all connected functions.
function Event:Fire(data)
	local it = self.listeners:Iterator()
	while (it:HasNext()) do
		it:Next()(data)
	end
end

-- Adds the given function to the list of functions to call when Fire is called.
function Event:Connect(f)
	self.listeners:Add(f)
end

return Event
