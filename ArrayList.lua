local Collection = require(script.Parent.Collection)
local List = require(script.Parent.List)
local Math = require(script.Parent.Math)

-- An ordered collection backed up by a lua table array
-- Note that just as other arrays in lua, a list is assumed
-- to have indexes starting at 1
local ArrayList = List:Subclass("ArrayList", function(this, elems)
    this.elements = {}
    if elems then this:AddAll(elems) end
end)

-- Add the given elem to this ArrayList. If no index is passed, the element
-- is added to the end of the array, otherwise it is inserted at the
-- given index, shifting all elements starting at that index to the right.
-- If index is not an integer or is not between 1 and the size + 1 inclusive
-- an exception will be thrown
function ArrayList:Add(elem, index)
    if index ~= nil then
        assert(Math.isint(index),
            "Index "..tostring(index).." is not an integer")
        assert(index > 0 and index <= self:Size() + 1,
            "Index "..index.." out of bounds with array size "
            ..self:Size())
        table.insert(self.elements, index, elem)
    else
        table.insert(self.elements, elem)
    end
end

-- Adds all the elements to this list. The elems parameter can be a Collection
-- or a lua table array. If index is passed, the elements are
-- inserted at the index, shifting all elements starting at that index
-- to the right as needed.
-- If index is not an integer or is not between 1 and the size + 1 inclusive
-- an exception will be thrown
function ArrayList:AddAll(elems, index)
    index = index or self:Size() + 1
    local it = Collection.iterator(elems)
    while it:HasNext() do
        self:Add(it:Next(), index)
        index = index + 1
    end
end

-- Returns the element at the given index.
function ArrayList:Get(index)
    assert(Math.isint(index),
            "Index "..tostring(index).." is not an integer")
    assert(index > 0 and index <= self:Size(),
            "Index "..index.." out of bounds with array size "
            ..self:Size())
    return self.elements[index]
end

-- Removes and returns the item at the given index. Will throw an
-- exception if the index is not an integer or is out of bounds
-- (less than 1 or greater than the size)
function List:Remove(index)
    assert(Math.isint(index),
            "Index "..tostring(index).." is not an integer")
    assert(index > 0 and index <= self:Size(),
            "Index "..index.." out of bounds with array size "
            ..self:Size())
    return table.remove(self.elements, index)
end

-- Returns the number of elements in the collection
function ArrayList:Size()
    return #self.elements
end

return ArrayList

-- -- Testing empty list constructor and Size()
-- local elist = ArrayList:new()
-- Object.print(elist)
-- Object.print(elist:Size())

-- -- Testing IsEmpty() and Add(elem)
-- Object.print(elist:IsEmpty())
-- elist:Add("Yo")
-- Object.print(elist:IsEmpty())

-- -- Testing Constructor initialized with elements
-- local alist = ArrayList:new({3,2,6,5,8})
-- Object.print(alist)

-- -- Testing Get(index)
-- Object.print("index 3 is ", alist:Get(3))
-- Object.print("index 5 is ", alist:Get(5))
-- local _, err = pcall(function()
--     return alist:Get(0)
-- end)
-- assert(err, "There should have been an error...")
-- print("ERROR: "..tostring(err))

-- local _, err = pcall(function()
--     return alist:Get(true)
-- end)
-- assert(err, "There should have been an error...")
-- print("ERROR: "..tostring(err))

-- -- Testing IndexOf(elem)
-- Object.print("6 is at index ", alist:IndexOf(6))
-- Object.print("10 is at index ", alist:IndexOf(10))

-- -- Testing Add(elem, index)
-- local midIdx = math.floor((alist:Size() + 1) / 2)
-- alist:Add("mid", midIdx)
-- Object.print("After adding mid at index", midIdx)
-- Object.print(alist)
-- local _, err = pcall(function()
--     alist:Add("truck", alist:Size() + 2)
-- end)
-- assert(err, "There should have been an error...")
-- print("ERROR: "..tostring(err))

-- local _, err = pcall(function()
--     alist:Add("truck", 0)
-- end)
-- assert(err, "There should have been an error...")
-- print("ERROR: "..tostring(err))

-- -- Testing AddAll(elems, index)
-- alist:AddAll({99, 78})
-- Object.print(alist)
-- alist:AddAll({"c", "a", "b"}, 5)
-- Object.print(alist)
-- alist:AddAll({"happy", "man"}, alist:Size() + 1)
-- Object.print(alist)
-- alist:AddAll({"beg", "in"}, 1)
-- Object.print(alist)

-- local _, err = pcall(function()
--     alist:AddAll({"truck", "apple"}, alist:Size() + 2)
-- end)
-- assert(err, "There should have been an error...")
-- print("ERROR: "..tostring(err))

-- -- Testing Remove(index)
-- local rem = alist:Remove(4)
-- Object.print("Removed item at index 4: ", rem)
-- Object.print(alist)
-- local _, err = pcall(function()
--     alist:Remove(alist:Size() + 1)
-- end)
-- assert(err, "There should have been an error...")
-- print("ERROR: "..tostring(err))
-- local _, err = pcall(function()
--     alist:Remove(0)
-- end)
-- assert(err, "There should have been an error...")
-- print("ERROR: "..tostring(err))
-- local _, err = pcall(function()
--     alist:Remove("Thing")
-- end)
-- assert(err, "There should have been an error...")
-- print("ERROR: "..tostring(err))

-- -- Testing Contains(elem)
-- Object.print("alist contains 5: ", alist:Contains(5))
-- Object.print("alist contains 345: ", alist:Contains(345))
-- Object.print("alist contains \"mid\": ", alist:Contains("mid"))

-- -- Testing ContainsAll(elems)
-- Object.print("alist contains {5, 6, 2}: ", alist:ContainsAll({5, 6, 2}))
-- Object.print("alist contains [5, 6, 2]: ", alist:ContainsAll(ArrayList:new({5, 6, 2})))
-- Object.print("alist contains {5, 6, 3}: ", alist:ContainsAll({5, 6, 3}))
-- Object.print("alist contains [5, 6, 3]: ", alist:ContainsAll(ArrayList:new({5, 6, 3})))

-- -- Testing ForEach(func)
-- alist:ForEach(function(elem)
--   Object.print("PROCESSING ", elem) 
-- end)

-- -- Testing ToArray()
-- Object.print(alist:ToArray())