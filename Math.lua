local Object = require(script.Parent.Object)

-- A class containing helpful math functions.
local Math = Object:Subclass("Math")

-- returns true if n is an integer and false otherwise
function Math.isint(n)
    return type(n) == "number" and n == math.floor(n)
end

-- Return the given num rounded to the given number of decimal places
-- if numDecimalPlaces is 0 or less, then the return value will
-- be an integer.
-- Examples:
-- Math.round(563.89764, 2) --> 563.9
-- Math.round(563.89764, 3) --> 563.898
-- Math.round(563.89764, 0) --> 564
-- Math.round(12312456, -2) --> 12312500
function Math.round(num, numDecimalPlaces)
    local mult = 10^(numDecimalPlaces or 0)
    local result
    if num >= 0 then result = math.floor(num * mult + 0.5) / mult
    else result = math.ceil(num * mult - 0.5) / mult end
    if mult <= 1 then result = math.floor(result) end
    return result
end

function Math.randomFloat(min, max)
	if min > max then
		local t = min
		min = max
		max = t
	end
	local diff = max - min
	return math.random() * diff + min
end

return Math
