local Object = require(script.Parent.Object)
local StringUtil = require(script.Parent.StringUtil)

local PathUtil = Object.Subclass("PathUtil")

-- Find a child from a roblox workspace path string such as:
-- "ServerStorage.Models.Weapons.IronSword"
function PathUtil.GetChild(path) 
	local pathSplit = StringUtil.split(path, ".")
	local cur = game
	for _,child in ipairs(pathSplit) do
		cur = cur:WaitForChild(child)
	end
	return cur
end

return PathUtil
