local Collection = require(script.Parent.Collection)
local Iterator = require(script.Parent.Iterator)

-- A List is an abstract class representing an ordered collection.
-- Note that just as other arrays in lua, a list is assumed
-- to have indexes starting at 1
local List = Collection:Subclass("List")

-- Returns the element at the given index.
function List:Get(index)
    assert(false, "Get must be implemented in subclasses of List")
end

function List:IndexOf(elem)
    local i = 1
    for i = 1, self:Size() do
        if self:Get(i) == elem then return i end
    end
    return -1
end

-- Removes and returns the item at the given index. Will throw an
-- exception if the index is not an integer or is out of bounds
-- (less than 1 or greater than the size)
function List:Remove(index)
    assert(false, "Remove must be implemented in subclasses of List")
end

-- ******************************************************* --
-- ******************** ListIterator ******************** --
-- ******************************************************* --
-- An Iterator that iterates over the values in a List
local ListIterator = Iterator:Subclass("ListIterator", function(this, list)
    this.list = list
    this.index = 1
end)

-- Returns the next element to process and increments next index.
-- Will throw an exception if there are no more elements to process.
function ListIterator:Next()
    assert(self:HasNext(), "No such element exists.")
    local val = self.list:Get(self.index)
    self.index = self.index + 1
    return val
end

-- Returns true if there is a next item to process and false otherwise.
function ListIterator:HasNext()
    return self.index <= self.list:Size()
end

-- Returns true if there is a previous item to process and false otherwise.
function ListIterator:HasPrevious()
    return self.index > 1
end

-- Returns the previous element and decrements next index.
-- Will throw an exception if there is no previous element.
function ListIterator:Previous()
    assert(self:HasPrevious(), "No such element exists.")
    self.index = self.index - 1
    local val = self.list:Get(self.index)
    return val
end

-- Returns the index of the item that will be returned by a call to Next()
function ListIterator:NextIndex()
    return self.index
end

-- Returns the index of the item that will be returned by a call to Previous()
function ListIterator:PreviousIndex()
    return self.index - 1
end

-- returns an iterator that can iterate over the List
function List:Iterator()
    return ListIterator:new(self)
end

return List