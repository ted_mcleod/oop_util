local Object = require(script.Parent.Object)
local Iterator = require(script.Parent.Iterator)

-- An abstract class representing a collection of objects.
local Collection = Object:Subclass("Collection")
    
-- Returns true if the given object is a Collection and false otherwise
function Collection.iscollection(o)
    return Object.isobj(o) and o:IsKindOfClass(Collection.ClassName)
end

-- Add the element to this collection.
function Collection:Add(elem)
    assert(false, "Add must be implemented in subclasses of Collection")
end

-- Add all the elements in the array to this collection
function Collection:AddAll(elems)
    local it = Collection.iterator(elems)
    while it:HasNext() do
        self:Add(it:Next())
    end
end

-- Returns true if the elem is contained in this collection
-- and false otherwise
function Collection:Contains(elem)
    local it = self:Iterator()
    while it:HasNext() do
        if it:Next() == elem then return true end
    end
    return false
end

-- Returns true if all the elements are contained in this collection
function Collection:ContainsAll(elems)
    local it = Collection.iterator(elems)
    while it:HasNext() do
        if not self:Contains(it:Next()) then return false end
    end
    return true
end

-- Returns true if this collection is empty and false otherwise
function Collection:IsEmpty()
    return self:Size() == 0
end

-- returns an iterator that can iterate over the collection
function Collection:Iterator()
    assert(false, "Iterator must be implemented in subclasses of Collection")
end

-- Returns the number of elements in the collection
function Collection:Size()
    assert(false, "Size must be implemented in subclasses of Collection")
end

-- For each element in this collection, the given function is called,
-- passing the element
function Collection:ForEach(func)
    local it = self:Iterator()
    while it:HasNext() do
        func(it:Next())
    end
end

function Collection:ToArray()
    local arr = {}
    local it = self:Iterator()
    local i = 1
    while it:HasNext() do
        arr[i] = it:Next()
        i = i + 1
    end
    return arr
end

-- Prints all the elements in the Collection, comma separated and enclosed in []:
-- [elem, elem, elem]
--Any references to itself will be printed as (self reference)
function Collection:ToString(multiline)
    multiline = multiline ~= nil and multiline or false
    local s = "["
    local it = self:Iterator()
    while it:HasNext() do
        local n = it:Next()
        if Object.isobj(n) then
            if n == self then
                s = s.."(self reference)"
            else
                s = s..n:ToString(multiline)
            end
        else
            s = s..Object.tostring(n, multiline)
        end
        if it:HasNext() then
            s = s..", "
            if multiline then s = s.."\n" end
        end
    end
    s = s.."]"
    return s
end

-- ******************************************************* --
-- ******************** ArrayIterator ******************** --
-- ******************************************************* --
-- This will assume the table is an array and will just iterate over the values
local ArrayIterator = Iterator:Subclass("ArrayIterator", function(this, arr)
    this.arr = arr
    this.index = 1
end)

-- Returns the next element to process. Will throw an exception if there
-- are no more elements to process.
function ArrayIterator:Next()
    assert(self:HasNext(), "No such element exists.")
    local val = self.arr[self.index]
    self.index = self.index + 1
    return val
end

-- Returns true if there is a next item to process and false otherwise.
function ArrayIterator:HasNext()
    return self.index <= #self.arr
end

-- Returns and Iterator for the given object. Throws an exception if
-- obj is either not a table or is an Object but not a Collection
function Collection.iterator(obj)
    assert(type(obj) == "table", tostring(obj).." is not a table.")
    if Collection.iscollection(obj) then return obj:Iterator() end
    assert(not Object.isobj(obj), Object.tostring(obj)
        .." is an Object of class "..tostring(obj)..", not a Collection, so cannot have an Iterator")
    return ArrayIterator:new(obj)
end

return Collection