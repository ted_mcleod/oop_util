local Object = require(script.Parent.Object)
local Collection = require(script.Parent.Collection)

-- Represents a Set of unique objects. Order is not stored, only presence.
-- Attempts to add the same item multiple times will be ignored.
local Set = Collection:Subclass("Set", function(this, elems)
    this.elements = {}
    this.size = 0
    if elems then this:AddAll(elems) end
end)

-- Add the object to this set. If the object is already in the set, this
-- will return false, otherwise it will return true and size will increase.
function Set:Add(obj)
    if self.elements[obj] == nil then
        self.elements[obj] = true
        self.size = self.size + 1
        return true
    end
    return false
end

-- Returns true if the object is contained in this Set and false otherwise.
function Set:Contains(obj)
    return self.elements[obj] ~= nil
end

-- Returns an lua table array containing all elements in the set
function Set:ToArray()
    local arr = {}
    local i = 1
    for k,v in pairs(self.elements) do
        arr[i] = k
        i = i + 1
    end
    return arr
end

-- Returns a set containing the elements contained in both this set AND setB
function Set:Intersection(setB)
    assert(Object.isobj(setB), "Invalid value passed to Intersection (not even an Object): "..tostring(setB))
    assert(setB:IsKindOfClass("Set"), "Intersection must be passed a Set, not a "..setB:GetClassName())
    local a = self
    local b = setB
    if self:Size() > setB:Size() then
        a = setB
        b = self
    end
    local i = Set:new()
    for k,v in pairs(a.elements) do
        if b:Contains(k) then i:Add(k) end
    end
    return i
end

-- Returns a set containing the elements contained in this set OR setB
function Set:Union(setB)
    assert(Object.isobj(setB), "Invalid value passed to Union (not even an Object): "..tostring(setB))
    assert(setB:IsKindOfClass("Set"), "Union must be passed a Set, not a "..setB:GetClassName())
    local u = Set:new(self)
    u:AddAll(setB)
    return u
end

-- Returns true if setB contains at least one element in common with this set
-- and false otherwise
function Set:Intersects(setB)
    assert(Object.isobj(setB), "Invalid value passed to Intersects (not even an Object): "..tostring(setB))
    assert(setB:IsKindOfClass("Set"), "Intersects must be passed a Set, not a "..setB:GetClassName())
    local a = self
    local b = setB
    if self:Size() > setB:Size() then
        a = setB
        b = self
    end
        
    for k,v in pairs(a.elements) do
        if b:Contains(k) then return true end
    end
    return false
end

-- Removes the given object from this Set. Returns false if the Set did not
-- contain the object, otherwise it decreases the size and returns true.
function Set:Remove(obj)
    if self.elements[obj] then
        self.elements[obj] = nil
        self.size = self.size - 1
        return true
    end
    return false
end

-- Returns the number of elements in this Set.
function Set:Size()
    return self.size
end

-- Returns an iterator to iterate over the elements of the set
function Set:Iterator()
    return Collection.iterator(self:ToArray())
end

return Set

-- Example Usage
-- print("Starting Set Test")
-- local s = Set:new()
-- s:Add(3)
-- s:Add(5)
-- s:Add(9)
-- Object.print(s)
-- print("s:Contains(3) = " .. tostring(s:Contains(3)))
-- print("s:Contains(4) = " .. tostring(s:Contains(4)))
-- local t = {car = "hi", bob = "no"}
-- local a = {car = "hi", bob = "no"}
-- local b = t
-- local ar1 = {1,2,3,4}
-- local ar2 = {1,2,3,4}
-- s:Add(ar1)
-- s:Add(t)
-- Object.print(s, Object.SINGLE_LINE)
-- Object.print(s)
-- print("s:Contains(t) = " .. tostring(s:Contains(t)))
-- print("s:Contains(a) = " .. tostring(s:Contains(a)))
-- print("s:Contains(b) = " .. tostring(s:Contains(b)))
-- print("s:Contains(ar1) = " .. tostring(s:Contains(ar1)))
-- print("s:Contains(ar2) = " .. tostring(s:Contains(ar2)))

-- local sa = Set:new({"A","J","P","N"})
-- local sb = Set:new({"G","A","Q","P", "U"})
-- local sc = Set:new({"M","T","L","Q", "K", "V"})
-- Object.print("sa: ", sa)
-- Object.print("sb: ", sb)
-- Object.print("sc: ", sc)
-- Object.print("sa Union sb:", sa:Union(sb))
-- Object.print("sa Intersects sb:", sa:Intersects(sb))
-- Object.print("sa Intersects sc:", sa:Intersects(sc))
-- Object.print("Intersection of sa and sc:", sa:Intersection(sc))
-- Object.print("Intersection of sb and sc:", sb:Intersection(sc))
-- Object.print("Intersection of sa and sb:", sa:Intersection(sb))
-- local _,err = pcall(function()
--     Object.print(sa:Intersects({"A","J","P","N"}))
-- end)
-- assert(err, "There should have been an error!")
-- print(err)
-- local _,err = pcall(function()
--     Object.print(sa:Intersects(ArrayList:new({"A","J","P","N"})))
-- end)
-- assert(err, "There should have been an error!")
-- print(err)
-- Object.print("removing Q from sc")
-- sc:Remove("Q")
-- Object.print("sc: ", sc)
-- Object.print("Intersection of sb and sc:", sb:Intersection(sc))
-- Object.print("sc is size ", sc:Size())
-- Object.print("Attempt to remove P from sc")
-- sc:Remove("P")
-- Object.print("sc is size ", sc:Size())
-- Object.print("Adding \"BOO\" to sc")
-- sc:Add("BOO")
-- Object.print("sc: ", sc)
-- Object.print("making sd a copy of sa using constructor parameter")
-- local sd = Set:new(sa)
-- Object.print("sd: ", sd)
-- Object.print("Removing J from sd")
-- sd:Remove("J")
-- Object.print("sd: ", sd)
-- Object.print("sa: ", sa)
-- local it = sa:Iterator()
-- while it:HasNext() do
--     Object.print(it:Next())
-- end