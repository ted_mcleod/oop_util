local Object = require(script.Parent.Object)

-- An abstract class representing an iterator that can iterate
-- over a collection or lua array
local Iterator = Object:Subclass("Iterator")

-- Returns the next element to process. Will throw an exception if there
-- are no more elements to process.
function Iterator:Next()
    assert(false, "Next must be implemented in subclasses of Iterator")
end

-- Returns true if there is a next item to process and false otherwise.
function Iterator:HasNext()
    assert(false, "HasNext must be implemented in subclasses of Iterator")
end

return Iterator