local Object = require(script.Parent.Object)
local Collection = require(script.Parent.Collection)

-- The Queue class represents a standard Queue (FIFO).
local Queue = Collection:Subclass("Queue", function(this)
    this.head = nil
    this.tail = nil
    this.size = 0
end)

-- Represents a node in the linked list that stores the queue data
local Node = Object:Subclass("Node", function(this, val, next, prev)
    this.next = next -- the node in front of this node in the queue
    this.prev = prev -- the node behind this node in the queue
    this.val = val -- the value stored in this node
end)

-- Add a value to the tail of the queue
function Queue:Add(elem)
    self:Enqueue(elem)
end

-- Returns the number of elements in the collection
function Queue:Size()
    return self.size
end

-- Add a value to the tail of the queue
function Queue:Enqueue(val)
    local node = Node:new(val, self.tail)
    if self.head == nil then self.head = node
    else self.tail.prev = node end
    self.tail = node
    self.size = self.size + 1
end

-- Remove and return the head of the queue
function Queue:Dequeue()
    local head = self.head
    if head == nil then return nil end
    self.head = self.head.prev
    if self.head ~= nil then self.head.next = nil end
    self.size = self.size - 1
    return head.val
end

-- Add an item to the head of the queue
function Queue:AddToFront(val)
    local node = Node:new(val, nil, self.head)
    if self.head == nil then self.head = node
    else self.head.next = node end
    self.head = node
    self.size = self.size + 1
end

-- Return the head of the queue without removing it
function Queue:Peek()
    if self.head == nil then return nil
    else return self.head.val end
end

local QueueIterator = Iterator:Subclass("QueueIterator", function(this, queue)
    this.queue = queue
    this.cur = queue.head
end)

-- Returns the next element to process. Will throw an exception if there
-- are no more elements to process.
function QueueIterator:Next()
    assert(self:HasNext(), "No such element")
    local n = self.cur
    self.cur = self.cur.prev
    return n.val
end

-- Returns true if there is a next item to process and false otherwise.
function QueueIterator:HasNext()
    return self.cur ~= nil
end

function Queue:Iterator()
    return QueueIterator:new(self)
end

return Queue

-- Example Usage
-- print("Starting QUEUE TEST")
-- local q = Queue:new()
-- q:Enqueue("A")
-- q:Enqueue("B")
-- q:Enqueue(q)
-- q:Enqueue(9)
-- print(q:ToString())
-- Object.print("Dequeuing results in: ", q:Dequeue())
-- Object.print("Queue now contains: ", q)
-- Object.print("Dequeuing results in: ", q:Dequeue())
-- Object.print("Queue now contains: ", q)
-- Object.print("Dequeuing results in: ", q:Dequeue())
-- Object.print("Queue now contains: ", q)
-- Object.print("Dequeuing results in: ", q:Dequeue())
-- Object.print("Queue now contains: ", q)
-- Object.print("Dequeuing results in: ", q:Dequeue())
-- Object.print("Queue now contains: ", q)
-- local _,err = pcall(function()
--     return q:Iterator():Next()
-- end)
-- assert(err, "There should have been an error...")
-- print("ERROR: "..tostring(err))

-- local q2 = Queue:new()
-- q2:Enqueue(8)
-- q2:Enqueue(3)
-- q2:Enqueue(55)
-- q2:Enqueue(34)
-- q2:Enqueue(7)
-- Object.print(q2)
-- Object.print("Peek() is "..q2:Peek())
-- Object.print("Size = "..q2:Size())
-- Object.print("Dequeuing results in: ", q2:Dequeue())
-- Object.print("Size = "..q2:Size())
-- Object.print("Queue now contains: ", q2)
-- q2:AddToFront(99)
-- Object.print("Queue now contains: ", q2)
-- q2:Enqueue(q)
-- q:Add(93)
-- q:Enqueue(31)
-- q:Enqueue(87)
-- Object.print("Queue now contains: ", q2)
-- Object.print("Size = "..q2:Size())

-- Object.print("q:ToArray() = ", q:ToArray())